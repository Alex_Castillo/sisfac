@extends('layouts.master')

@section('title','Gestion de Facturas')

@section('sidebar')
@parent
@endsection
@section('content')
<div class="container-fluid">
	
<br>
<h1 class="text-center text-white bg-dark"><i class="fas fa-file-invoice-dollar"></i> DATOS DE LAS FACTURAS.</h1>
<br>
	<!-- Button trigger modal -->
	<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#insFac">
		<i class="fas fa-feather-alt"></i> Registrar nueva factura</button>
<br>
<br>
<table id="datos" class="table table-hover table-striped table-sm table-bordered">
	<thead class="thead-dark text-center">
		<th>N°</th>
		<th>Correlativo</th>
		<th>Empleado</th>
		<th>Cliente</th>
		<th>Fecha</th>
		<th>Modo de pago</th>
		<th>Total</th>
		<th>Detalles</th>		
	</thead>
	<tbody  class="text-center">
		<?php $n=1; ?>
			@foreach ($facturas as $f) 
			<tr>
					<td style="width: 3%">
						<?php echo $n; $n++; ?>
					</td>
					<td style="width: 8%">
						{{ $f->correlativo }}
					</td>
					<td style="width: 20%">
						{{ $f->name_e.' '.$f->lastname_e }}
					</td>
					<td style="width: 20%">
						{{ $f->name_c.' '.$f->lastname_c }}
					</td>
					<td style="width: 9%">
						{{ $f->fecha }}
					</td>
					<td style="width: 12%">
						{{ $f->modo }}
					</td>
					<td style="width: 12%">
						{{ $f->total }}
					</td>
					<td style="width: 4%"><h1><i class="fas fa-chart-line"  style="color: #0DFC13"></i></h1></td>

			</tr>
		@endforeach
	</tbody>
</table>

	<!-- Modal -->
	<div class="modal fade" id="insFac" tabindex="-1" role="dialog" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-plus-circle"></i> Nuevo cliente</h5>
				</div>
				<div class="modal-body">
					<form method="POST" action="#">
						<!-- Anti-ataques CSRF -->
						{{csrf_field()}}
						
						<div class="modal-footer">
							<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-outline-primary">Guardar cambios</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">

		$(document).ready(function() {
			$('#datos').DataTable( {
				"ordering": false,
				"info":     false
			} );
		} );
	</script>
</div>
@endsection