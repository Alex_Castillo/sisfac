<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<!-- bootstrap estilos -->
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
	<!-- mis  pocos estilos -->
	<link rel="stylesheet" type="text/css" href="{{asset('css/hola.css')}}">
	<!-- cdn datatables.css -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
	<!-- iconos fontawesome -->
	<script src="https://kit.fontawesome.com/f65d5aecba.js" crossorigin="anonymous"></script>
	<!-- library jquey -->
	<script type="text/javascript" src="{{asset('js/jquery-3.3.1.slim.js')}}"></script>
	<!-- popper -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<!-- library bootstrap.js -->
	<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
	<!-- library inputmask.js -->
	<script type="text/javascript" src="{{asset('js/inputmask.js')}}"></script>
	<!-- cdn datatables.js -->
	<script type="text/javascript"src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
	<!-- cdn jquery-3.3.1.slim.js -->
	<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
	<!-- cdn bootstrap.min.js v4 -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->






	<div class="container-fluid">
		@section('sidebar')
		<div class="navbar navbar-expand-lg navbar-outline-dark" style="border: 4px solid black">
			<a class="navbar-brand" href="#"><h3><!-- <i class="fas fa-store"></i> --><i class="fas fa-store-alt"></i> Electrodomestic Store</h3></a>

			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<div class="navbar-nav">
					<a class="nav-item nav-link active" href="{{url('/')}}">Clientes</a>
					<a class="nav-item nav-link" href="{{url('/Facturas/')}}">Facturas</a>
					<a class="nav-item nav-link" href="{{url('/Creditos/')}}">Creditos</a>
				</div>
			</div>
			<form class="form-inline my-2 my-lg-0">
				<h2><a href="#" class="btn"><h2><i class="fas fa-user-circle" style="color: black"></i></h2></a></h2>
			</form>

		</div>
		<div class="col-sm-12">

			@if(session()->get('success'))
			<div class="alert alert-success">
				{{ session()->get('success') }}  
			</div>
			@endif
		</div>
	</div>

	@show
</head>
<body>
	
	@yield('content')


	@show
</body>
</html>