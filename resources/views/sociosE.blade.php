@extends('layouts.master')

@section('title','Actualizacion de Clientes')

@section('sidebar')
@parent
@endsection
@section('content')

<br>
<h1 class="text-center text-white bg-dark">ACTUALIZANDO DATOS DE CLIENTES.</h1>
<br>

<?php foreach ($clientes as $c): ?>
	<form method="get" action="{{Route('actualizar',$c->id_cliente)}}">
		<!-- Anti-ataques CSRF -->
		{{csrf_field()}}
		<div class="row">
			<div class="col-md-6">
				<label>
					Ingrese los nombres del cliente:
				</label>
			</div>
			<div class="col-md-6">
				<input type="text" name="name" class="form-control" id="name" value="<?php echo $c->name_c ?>">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<label>
					Ingrese los apellidos del cliente:
				</label>
			</div>
			<div class="col-md-6">
				<input type="text" name="lastname" class="form-control" id="lastname" value="<?php echo $c->lastname_c ?>">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<label>
					Ingrese la fecha de nacimiento:
				</label>
			</div>
			<div class="col-md-6">
				<input type="date" name="birthday" class="form-control" max="<?php echo Date('Y-m-d'); ?>" id="birthday" value="<?php echo $c->birthday ?>">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<label>
					Ingrese el numero de documento unico de identidad del cliente:
				</label>
			</div>
			<div class="col-md-6">
				<input type="text" name="dui" class="form-control" maxlength="10" minlength="10" id="dui" value="<?php echo $c->dui ?>">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<label>
					Ingrese el contacto telefonico del cliente:
				</label>
			</div>
			<div class="col-md-6">
				<input type="text" name="phone" maxlength="9" minlength="9" class="form-control" id="phone" value="<?php echo $c->phone ?>">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<label>
					Ingrese el correo electronico del cliente:
				</label>
			</div>
			<div class="col-md-6">
				<input type="text" name="email" maxlength="50" class="form-control" id="email" value="<?php echo $c->email ?>">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6">
				<label>
					Ingrese la direccion del cliente:
				</label>
			</div>
			<div class="col-md-6">
				<textarea name="direccion" class="form-control" id="direccion" >
					<?php echo $c->direccion ?>
				</textarea>
			</div>
		</div>
		<br>

		<a href="{{Route('cliente')}}" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</a>
		<button type="submit" class="btn btn-outline-primary">Guardar cambios</button>
	</form>

<?php endforeach ?>
<script type="text/javascript">
	$(document).ready(function(){
		var win = $("#dui");
		var lose = new Inputmask("99999999-9");
		lose.mask(win);
	});
	$(document).ready(function(){
		var dog = $("#phone");
		var cat = new Inputmask("9999-9999");
		cat.mask(dog);
	});
	
</script>
@endsection