@extends('layouts.master')

@section('title','Gestion de Clientes')

@section('sidebar')
@parent
@endsection
@section('content')
<div class="container-fluid">
	
	<br>
	<h1 class="text-center text-white bg-dark"><i class="fas fa-address-book"></i> DATOS DE LOS CLIENTES.</h1>
	<br>
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div><br />
	@endif
	<!-- Button trigger modal -->
	 <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#insClient">
		<i class="fas fa-feather-alt"></i> Nuevo cliente
	</button>
	<br>
	<br>
	<table id="datos" class="table table-hover table-striped table-sm table-bordered">
		<thead class="thead-dark text-center">
			<th>N°</th>
			<th>Nombre del cliente</th>
			<th>DUI</th>
			<th>Fecha de nacimiento</th>
			<th>Telefono</th>
			<th>E-mail</th>
			<th>Direccion</th>
			<th>Fac.</th>
			<th>Cred.</th>
			<th>Edit.</th>
			<th>Del.</th>
		</thead>
		<tbody class="text-center">
			<?php $n=1; ?>
			@foreach ($clientes as $c) 
			<tr>
				<td style="width: 3%"><?php echo $n; $n++; ?></td>
				<td style="width: 18%">{{ $c->name_c.' '.$c->lastname_c }}</td>
				<td style="width: 10%">{{ $c->dui }}</td>
				<td style="width: 7%">{{ $c->birthday }}</td>
				<td style="width: 7%">{{ $c->phone }}</td>
				<td style="width: 18%">{{ $c->email }}</td>
				<td style="width: 35%">{{ $c->direccion }}</td>
				<td  style="width: 2%" data-toggle="tooltip" data-placement="left" title="{{ '¡Facturas del cliente '.$c->name_c.' '.$c->lastname_c.'!' }}"><h4><i class="far fa-file-alt" style="color: #F8A300"></i></h4></td>
				<td  style="width: 2%"  data-toggle="tooltip" data-placement="left" title="{{ '¡Creditos del cliente '.$c->name_c.' '.$c->lastname_c.'!' }}"><h4><i class="fas fa-credit-card" style="color: #4551EC"></i></h4></td>
				<td  style="width: 2%"  data-toggle="tooltip" data-placement="left" title="{{'Editar registro '.$c->dui }}"><a href="{{Route('editar',$c->id_cliente)}}"><h4><i class="far fa-edit" style="color: #15EF58"></i></h4></a></td>
				<td  style="width: 2%"  data-toggle="tooltip" data-placement="left" title="{{'Eliminar registro '.$c->dui }}" data="{{ $c->id_cliente }}"><a href="{{Route('eliminar',$c->id_cliente)}}"><h4><i class="fas fa-trash-alt" style="color: #FF0808"></i></h4></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>

	<!-- Modal -->
	<div class="modal fade" id="insClient" tabindex="-1" role="dialog" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-plus-circle"></i> Nuevo cliente</h5>
				</div>
				<div class="modal-body">
					<form method="POST" action="{{Route('newClient')}}">
						<!-- Anti-ataques CSRF -->
						{{csrf_field()}}
						<div class="row">
							<div class="col-md-6">
								<label>
									Ingrese los nombres del cliente:
								</label>
							</div>
							<div class="col-md-6">
								<input type="text" name="name" class="form-control" id="name">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6">
								<label>
									Ingrese los apellidos del cliente:
								</label>
							</div>
							<div class="col-md-6">
								<input type="text" name="lastname" class="form-control" id="lastname">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6">
								<label>
									Ingrese la fecha de nacimiento:
								</label>
							</div>
							<div class="col-md-6">
								<input type="date" name="birthday" class="form-control" max="<?php echo Date('Y-m-d'); ?>" id="birthday">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6">
								<label>
									Ingrese el numero de documento unico de identidad del cliente:
								</label>
							</div>
							<div class="col-md-6">
								<input type="text" name="dui" class="form-control" maxlength="10" minlength="10" id="dui">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6">
								<label>
									Ingrese el contacto telefonico del cliente:
								</label>
							</div>
							<div class="col-md-6">
								<input type="text" name="phone" maxlength="9" minlength="9" class="form-control" id="phone">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6">
								<label>
									Ingrese el correo electronico del cliente:
								</label>
							</div>
							<div class="col-md-6">
								<input type="text" name="email" maxlength="50" class="form-control" id="email">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6">
								<label>
									Ingrese la direccion del cliente:
								</label>
							</div>
							<div class="col-md-6">
								<textarea name="direccion" class="form-control" id="direccion">

								</textarea>
							</div>
						</div>
						<br>
						<div class="modal-footer">
							<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-outline-primary">Guardar cambios</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
			<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Modal -->
					<div class="modal fade" id="delCliente" tabindex="-1" role="dialog">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title"><i class="fas fa-user-times"></i> Eliminar Registro</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<img src="{{ asset('images/icons/eliminar.png')}}" alt="" class="animated infinite tada delay-1s alertas img-fluid" width="25%" height="25%"><br>
									<h3 class="text-center">¿Desea eliminar el registro?</h3>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-outline-danger centrado" id="btnBorrar" data-dismiss="modal">Eliminar</button>
									<button type="button" class="btn btn-outline-info centrado" data-dismiss="modal">Cancelar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<script type="text/javascript">
		$(function () {
			$('[data-toggle="tooltip"]').tooltip();
		})
		$(document).ready(function() {
			$('#datos').DataTable( {
				"ordering": false,
				"info":     false
			} );
		} );
		$(document).ready(function(){
			var win = $("#dui");
			var lose = new Inputmask("99999999-9");
			lose.mask(win);
		});
		$(document).ready(function(){
			var dog = $("#phone");
			var cat = new Inputmask("9999-9999");
			cat.mask(dog);
		});

	</script>
</div>
@endsection