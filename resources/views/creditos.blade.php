@extends('layouts.master')

@section('title','Gestion de Creditos')

@section('sidebar')
@parent
@endsection
@section('content')
<div class="container-fluid">
	
<br>
<h1 class="text-center text-white bg-dark"><i class="fas fa-hourglass-half"></i></i> DATOS DE CREDITOS.</h1>
<br>
	<!-- Button trigger modal -->
	<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#insCre">
		<i class="fas fa-feather-alt"></i> Registrar nuevo credito</button>
<br>
<br>
<table id="datos" class="table table-hover table-striped table-sm table-bordered">
	<thead class="thead-dark text-center">
		<th>N°</th>
		<th>Correlativo</th>
		<th>Empleado</th>
		<th>Cliente</th>
		<th>Modo de pago</th>
		<th>Fecha de inicio</th>
		<th>Fecha finalizacion</th>
		<th>Detalles</th>
		<th>Seguimiento</th>		
	</thead>
	<tbody>
			<tr>
				<tbody class="text-center">
					<td style="width: 3%"></td>
					<td style="width: 8%"></td>
					<td style="width: 20%"></td>
					<td style="width: 20%"></td>
					<td style="width: 9%"></td>
					<td style="width: 9%"></td>
					<td style="width: 9%"></td>
					<td style="width: 4%"><h1><i class="fas fa-chart-line"  style="color: #0DFC13"></i></h1></td>
					<td style="width: 4%"><h1><i class="fas fa-share"  style="color: #0D83FC"></i></h1></td>
				</tbody>
			</tr>
	</tbody>
</table>

	<!-- Modal -->
	<div class="modal fade" id="insCre" tabindex="-1" role="dialog" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-plus-circle"></i> Nuevo cliente</h5>
				</div>
				<div class="modal-body">
					<form method="POST" action="#">
						<!-- Anti-ataques CSRF -->
						{{csrf_field()}}
						
						<div class="modal-footer">
							<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-outline-primary">Guardar cambios</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection