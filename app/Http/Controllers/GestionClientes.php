<?php

namespace App\Http\Controllers;

use App\model\Clientes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GestionClientes extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     *fn index[Cargar vista inicial de este controller]
     * @param na
     * @return [inf tb clientes]
     */
    public function index(){
        $k = Clientes::all();
        echo view('socios',['clientes' => $k]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $request->validate([
            'dui'=>'required',
            'name'=>'required',
            'lastname'=>'required',
            'direccion'=>'required',
            'birthday'=>'required',
            'phone'=>'required',
            'email'=>'required'
        ]);
        $data = request()->all();
        $k = new Clientes;
        $k->dui = $data['dui'];
        $k->name_c = $data['name'];
        $k->lastname_c = $data['lastname'];
        $k->direccion = $data['direccion'];
        $k->birthday = $data['birthday'];
        $k->phone = $data['phone'];
        $k->email = $data['email'];
        $k->save();
        return redirect()->URL('cliente')->with('success', 'Nuevo cliente agregado!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function show(Clientes $clientes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function edit(Clientes $id)
    {
        $k = $id->find($id);
        echo view('sociosE',['clientes' => $k]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, /*Clientes*/ $id){
           //
        $request->validate([
            'dui'=>'required',
            'name'=>'required',
            'lastname'=>'required',
            'direccion'=>'required',
            'birthday'=>'required',
            'phone'=>'required',
            'email'=>'required'
        ]);
         $k = Clientes::find($id);
        $k->dui =  $request->get('dui');
        $k->name_c = $request->get('name');
        $k->lastname_c = $request->get('lastname');
        $k->direccion = $request->get('direccion');
        $k->birthday = $request->get('birthday');
        $k->phone = $request->get('phone');
        $k->email = $request->get('email');
        $k->save();
       
        return redirect()->URL('cliente')->with('success', 'Cliente actualizado!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function destroy(/**Clientes*/ $id){
        //
     DB::table('clientes')->where('id_cliente', '=', $id)->delete();
     return redirect()->URL('cliente')->with('success', 'Cliente eliminado!');
 }
}
