<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Views inicio
Route::get('/','GestionClientes@index')->name('cliente');
//Insertar nuevo cliente
Route::post('/Clientes/nuevo','GestionClientes@create')->name('newClient');
//Eliminar cliente
Route::get('/Clientes/eliminar/{id_cliente}','GestionClientes@destroy')->where('id_cliente','[0-9]+')->name('eliminar');
//Editar
Route::get('/Clientes/editar/{id}','GestionClientes@edit')->where('id_cliente','[0-9]+')->name('editar');
// actualizar
Route::get('/Clientes/update/{id}','GestionClientes@update')->where('id_cliente','[0-9]+')->name('actualizar');
// Views inicio
Route::get('/Facturas/','GestionFacturas@index')->name('factura');
// Views inicio
Route::get('/Creditos/','GestionCreditos@index')->name('credito');