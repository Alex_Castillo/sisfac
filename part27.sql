-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-02-2020 a las 22:50:39
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `part27`
--
CREATE DATABASE IF NOT EXISTS `part27` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `part27`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `mostrarC` ()  BEGIN
SELECT c.id_cliente,c.dui,c.lastname_c,c.direccion,c.birthday,c.phone,c.email  FROM clientes c;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mostrarF` ()  BEGIN
SELECT f.correlativo,e.name_e,e.lastname_e,c.name_c,c.lastname_c,f.fecha,m.modo FROM facturas f INNER JOIN empleados e on e.id_empleado = f.id_empleado INNER JOIN clientes c on c.id_cliente=f.id_cliente INNER JOIN modo_pago m on m.id_pago = f.id_pago WHERE m.modo='al contado';
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `name_cat` varchar(45) NOT NULL,
  `descripcion_cat` varchar(250) NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `name_cat`, `descripcion_cat`) VALUES
(1, 'Linea blanca', 'Principales electrodomésticos vinculados a la cocina y limpieza del hogar. '),
(2, 'Linea marrón', 'Hace referencia al conjunto de electrodomésticos de video y audio.'),
(3, 'Mantenimiento de la casa', 'Productos electrodoméstico orientados a la limpieza del hogar.'),
(4, 'Preparación de alimentos', 'Productos electrodoméstico orientados a la preparación de alimentos.'),
(5, 'Higiene y belleza', 'Productos electrodoméstico orientados a la higiene y belleza personal.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `dui` varchar(10) NOT NULL,
  `name_c` varchar(50) NOT NULL,
  `lastname_c` varchar(50) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `birthday` date NOT NULL,
  `phone` varchar(9) NOT NULL,
  `email` varchar(65) NOT NULL,
  PRIMARY KEY (`id_cliente`),
  UNIQUE KEY `dui` (`dui`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `dui`, `name_c`, `lastname_c`, `direccion`, `birthday`, `phone`, `email`) VALUES
(1, '01234567-8', 'Gabriela Sarahi', 'Aguilar Hernandez', 'su casa donde vive su familia a la par de la casa de la vecina enfrente de una calle', '1998-07-21', '2175-7575', 'gabriela.aguilar@gmail.com'),
(2, '98765432-1', 'Kenia Susana', 'Campos Castro', 'su casa donde viven sus papas y su hermano', '1997-12-12', '6164-7918', 'susana-campos@gmail.com'),
(3, '00312245-9', 'Angela Veronica ', 'Pineda Miranda', 'su casa x3', '2020-02-02', '7915-6162', 'verito.miranda@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `creditos`
--

CREATE TABLE IF NOT EXISTS `creditos` (
  `correlativo_c` varchar(7) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_pago` int(11) NOT NULL,
  `fecha_ini` date NOT NULL,
  `fecha_fin` date NOT NULL,
  PRIMARY KEY (`correlativo_c`),
  KEY `id_empleado` (`id_empleado`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_pago` (`id_pago`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `creditos`
--

INSERT INTO `creditos` (`correlativo_c`, `id_empleado`, `id_cliente`, `id_pago`, `fecha_ini`, `fecha_fin`) VALUES
('CRE0001', 1, 2, 3, '2020-01-30', '2020-05-30'),
('CRE0002', 1, 1, 3, '2020-01-30', '2020-04-30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles`
--

CREATE TABLE IF NOT EXISTS `detalles` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `correlativo` varchar(7) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id_detalle`,`correlativo`),
  KEY `id_producto` (`id_producto`),
  KEY `correlativo` (`correlativo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalles`
--

INSERT INTO `detalles` (`id_detalle`, `correlativo`, `id_producto`, `cantidad`, `precio`, `total`) VALUES
(1, 'PHP0001', 17, 2, '112.00', '224.00'),
(2, 'PHP0002', 17, 1, '56.00', '56.00'),
(3, 'PHP0003', 17, 2, '112.00', '224.00'),
(4, 'PHP0003', 19, 3, '180.00', '0.00'),
(5, 'PHP0003', 20, 1, '335.00', '0.00'),
(6, 'PHP0002', 17, 2, '112.00', '0.00'),
(7, 'PHP0002', 19, 3, '180.00', '0.00'),
(8, 'PHP0002', 20, 1, '335.00', '0.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_creditos`
--

CREATE TABLE IF NOT EXISTS `detalles_creditos` (
  `id_detallesc` int(11) NOT NULL AUTO_INCREMENT,
  `correlativo_c` varchar(7) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id_detallesc`,`correlativo_c`),
  KEY `correlativo` (`correlativo_c`),
  KEY `id_producto` (`id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalles_creditos`
--

INSERT INTO `detalles_creditos` (`id_detallesc`, `correlativo_c`, `id_producto`, `cantidad`, `precio`) VALUES
(1, 'CRE0001', 20, 2, '670.00'),
(2, 'CRE0001', 15, 3, '87.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE IF NOT EXISTS `empleados` (
  `id_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `dui` varchar(10) NOT NULL,
  `name_e` varchar(50) NOT NULL,
  `lastname_e` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `phone` varchar(9) NOT NULL,
  `email` varchar(65) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  PRIMARY KEY (`id_empleado`),
  UNIQUE KEY `dui` (`dui`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id_empleado`, `dui`, `name_e`, `lastname_e`, `birthday`, `phone`, `email`, `direccion`) VALUES
(1, '05867414-6', 'Isaac Alexander ', 'Castillo Lopez', '1999-04-21', '7915-6162', 'castillo.lopez2199@gmail.com', 'casa numero 3 Comunidad San Lorenzo km 50, Carretera hacia Costa del Sol, Canton El Pedregal, El Rosario, La Paz');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE IF NOT EXISTS `facturas` (
  `correlativo` varchar(7) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_pago` int(11) NOT NULL,
  PRIMARY KEY (`correlativo`),
  KEY `id_pago` (`id_pago`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_empleado` (`id_empleado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`correlativo`, `id_empleado`, `id_cliente`, `fecha`, `id_pago`) VALUES
('PHP0001', 1, 2, '2020-01-29', 1),
('PHP0002', 1, 2, '2020-01-29', 3),
('PHP0003', 1, 3, '2020-02-03', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modo_pago`
--

CREATE TABLE IF NOT EXISTS `modo_pago` (
  `id_pago` int(11) NOT NULL AUTO_INCREMENT,
  `modo` varchar(50) NOT NULL,
  `otro_detalle` varchar(45) NOT NULL,
  PRIMARY KEY (`id_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `modo_pago`
--

INSERT INTO `modo_pago` (`id_pago`, `modo`, `otro_detalle`) VALUES
(1, 'al contado', 'Solo billetes estadounidense.'),
(3, 'credito', 'Credito con plazo de 6 a 12 meses');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `name_p` varchar(45) NOT NULL,
  `modelo` varchar(45) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `stock` int(11) NOT NULL,
  PRIMARY KEY (`id_producto`),
  KEY `id_categoria` (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `name_p`, `modelo`, `id_categoria`, `descripcion`, `precio`, `stock`) VALUES
(1, 'Pantalla plana Sharp 40 pulgadas', 'LC-40Q3070U', 2, 'Sharp 40\" Class FHD (1080p) LED TV (LC-40Q3070U)', '179.99', 50),
(2, 'Televisor LG Class 55 pulgadas', '55UH615A', 2, 'Televisor pantalla plana LG Class de 55″ y más de 2160 megapíxeles, con tecnología 4K Ultra HD Smart LED LCD. Posee conectividad 4 HDMI, USB y Ethernet.', '1299.99', 80),
(3, 'Equipo de Sonido LG', 'CJ88', 2, 'Equipo de sonido con multi Bluetooth, 2 entradas USB. Disfruta de un potente y exclusivo equipo de sonido LG, para ambientar tu hogar con la mejor melodía', '499.00', 60),
(4, 'Equipo de Sonido LG CM-4360', 'CM-4360', 2, 'Equipo de sonido de 230W – RMS, Bluetooth y Auto DJ. Disfruta de un potente sonido gracias a sus altavoces de doble vía y subwoofer integrado.', '129.00', 80),
(5, 'Cocina Cetron CC20TB-O', 'CC20TB-O', 1, 'Cocina de mesa de 20\" con 4 quemadores, color blanco, con cubierta porcenalizada.', '75.00', 60),
(6, 'Cocina Mabe WEM5044CAIBO', 'WEM5044CAIBO', 1, 'Cocina a gas de 20\" cubierta de acero inoxidable, Sus diseños amplios y perfecta distribución de hornillas le permitirá cocinar varios platillos al mismo tiempo.', '265.00', 75),
(7, 'Congelador Fogel FREEZ-7-HC', 'FREEZ-7-HC', 1, 'Exhibidor de helados con frente inclinado y puertas de vidrio curvo, ideal para tu negocio conservan la frescura de tus helados.', '950.00', 75),
(8, 'Ventilador de Piso Midea', 'MZBF80D2DB', 1, 'Ventilador de piso de 20\", 3 velocidades, cabeza ajustable.', '45.00', 60),
(9, 'Aspiradora LG', 'VH9200DS ', 3, 'Diseño que permite una fácil limpieza gracias las 11 capas de fibras entrelazadas del filtro HEPA que atrapan y retienen la mugre.', '305.00', 45),
(10, 'Aspiradora de mano Black + Decker', 'HWVI- 225J01', 3, 'Práctica y liviana aspiradora de mano con batería de Lithium, nos asegura mayor succión y duración. Para aspirar en seco o mojado ¡Ideal para las tareas de limpieza!', '66.00', 85),
(11, 'Plancha Black + Decker', 'IR-1830', 3, 'Plancha con tecnología Smart Steam,genera potente emisión de vapor automáticamente en temperaturas altas, con función de rocio.', '28.00', 40),
(12, 'Plancha Black + Decker IRBD200', 'IRBD200', 3, 'Plancha de Vapor con suela TrueGlide que alisa arrugas rápidamente', '21.00', 60),
(13, 'Cafetera Black + Decker', 'CM0941B', 4, 'Cafetera de 12 tazas, prepara rápidamente tu café favorito con sólo tocar un botón. Jarra de virio Duralife y filtro permanente lavable.', '36.00', 45),
(14, 'Cafetera Black + Decker CM618', 'CM618', 4, 'Cafetera personal versátil y conveniente, funciona perfecto con bolsitas de café autofiltrantes y café molido.', '22.99', 75),
(15, 'Sandwichera Black + Decker', 'SM24520', 4, 'La Sandwichera de Relleno Profundo de 2 Porciones B+D está lista para llevar sus creaciones de sándwiches a un nuevo nivel.', '29.00', 50),
(16, 'Tostador de pan Windmere', 'TR200WM ', 4, 'Tostador moderno y fácil de usar', '17.00', 57),
(17, 'Afeitadora Remington', 'PG6025 ', 5, 'Avanzado Kit de corte Remington PG6025 para barba de caballero, todo en 1.', '56.00', 65),
(18, 'Alisadora Remington', 'S7320 ', 5, 'Alisadora Remington Wet2straight S7320 con placas de cerámica avanzada infundidas con Sal de Mar.', '74.00', 25),
(19, 'Alisadora de cerámica Remington', 'S-8001', 5, 'Alisadora con revestimiento de cerámica con micro-acondicionadores anti-encrespamiento. para uso en cabello húmedo y seco.', '60.00', 80),
(20, 'Depiladora de luz pulsada intensa Remington', 'IPL6000 ', 5, 'Un exclusivo sistema que aprovecha la tecnología de luz pulsada intensa, la misma utilizada en los institutos de belleza y clínicas para la eliminación efectiva y segura del vello en el hogar. Este sistema es apto para hombres y mujeres.', '335.00', 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguimientos`
--

CREATE TABLE IF NOT EXISTS `seguimientos` (
  `id_seguimiento` int(11) NOT NULL AUTO_INCREMENT,
  `correlativo_c` varchar(7) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `restante` decimal(10,2) NOT NULL,
  `abono` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id_seguimiento`,`correlativo_c`),
  KEY `correlativo_c` (`correlativo_c`),
  KEY `id_empleado` (`id_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `seguimientos`
--

INSERT INTO `seguimientos` (`id_seguimiento`, `correlativo_c`, `id_empleado`, `fecha`, `restante`, `abono`) VALUES
(1, 'CRE0002', 1, '2020-02-04', '550.00', '26.00');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `creditos`
--
ALTER TABLE `creditos`
  ADD CONSTRAINT `creditos_ibfk_1` FOREIGN KEY (`id_empleado`) REFERENCES `empleados` (`id_empleado`),
  ADD CONSTRAINT `creditos_ibfk_2` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`),
  ADD CONSTRAINT `creditos_ibfk_3` FOREIGN KEY (`id_pago`) REFERENCES `modo_pago` (`id_pago`);

--
-- Filtros para la tabla `detalles`
--
ALTER TABLE `detalles`
  ADD CONSTRAINT `detalles_ibfk_1` FOREIGN KEY (`correlativo`) REFERENCES `facturas` (`correlativo`),
  ADD CONSTRAINT `detalles_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`);

--
-- Filtros para la tabla `detalles_creditos`
--
ALTER TABLE `detalles_creditos`
  ADD CONSTRAINT `detalles_creditos_ibfk_1` FOREIGN KEY (`correlativo_c`) REFERENCES `creditos` (`correlativo_c`),
  ADD CONSTRAINT `detalles_creditos_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`);

--
-- Filtros para la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD CONSTRAINT `facturas_ibfk_1` FOREIGN KEY (`id_pago`) REFERENCES `modo_pago` (`id_pago`),
  ADD CONSTRAINT `facturas_ibfk_2` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`),
  ADD CONSTRAINT `facturas_ibfk_3` FOREIGN KEY (`id_empleado`) REFERENCES `empleados` (`id_empleado`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`);

--
-- Filtros para la tabla `seguimientos`
--
ALTER TABLE `seguimientos`
  ADD CONSTRAINT `seguimientos_ibfk_1` FOREIGN KEY (`correlativo_c`) REFERENCES `creditos` (`correlativo_c`),
  ADD CONSTRAINT `seguimientos_ibfk_2` FOREIGN KEY (`id_empleado`) REFERENCES `empleados` (`id_empleado`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
